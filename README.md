# User Management System
This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## How to start the App
Before run the App, we should run the backend service localy\
Here is the repository for API: [API Repository](https://gitlab.com/MaximillianoNico/user-service-fill-labs)
<br/>
<br/>

First, run the development server:

```bash
cp .env.dist .env

yarn install
yarn dev
```

Open [Login Page](http://localhost:3000/login) with your browser to see the result.

### Here is the API Documentation
[API Documentation](https://go.postman.co/workspace/My-Workspace~4aaae94a-9394-4fcf-8cbe-23ea4f1daa6d/collection/4055949-5bdcbc90-3ae4-4629-937d-98e37ef7d3d8?action=share&creator=4055949)

## Authorization
Currently we have public and private api. the private api required authorization on the headers.
for authorization we using JWT
```
Bearer {{token_jwt}}
```

## Response
```json
{
    "status": "success | errors",
    "data": {},
    "message": "Message"
}
```