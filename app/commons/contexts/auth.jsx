import { getCookie } from "cookies-next";
import { useRouter } from "next/router";
import React, { createContext, useEffect, useState } from "react";
import Loader from "../../components/Loaders";

export const AuthContext = createContext(null);

export const AuthProvider = ({ children, pageProps, ssrProps }) => {
  console.log('ssrProps: ', ssrProps)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(true);
  const csr = getCookie('tkn')

  useEffect(() => {
    if (pageProps?.protected && !(ssrProps?.authToken || csr)) {
      setTimeout(() => {
        router.push("/login")
      }, 1500)

      return;
    }

    setIsLoading(false);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageProps?.protected, ssrProps?.authToken])


  if (pageProps?.protected && isLoading) return <Loader />

  return (
    <AuthContext.Provider value={{ value: { isLoading } }}>
      {children}
    </AuthContext.Provider>
  )
}