import awaitTo from '../libs/awaitTo'
import Fetch from './index'

export const SignIn = async (email, password) => {
  const Req = () => Fetch.post("/auth/sign-in", {
    email,
    password
  });
  const [err, response] = await awaitTo(Req)

  return { err, response }
}
