import axios from 'axios'
import { getCookie } from 'cookies-next';

const Request = () => {
  const instance = axios.create({
    baseURL: process.env.API_HOST || "http://localhost:3010"
  });
  
  const AUTH_TOKEN = getCookie('tkn') || null
  // Alter defaults after instance has been created
  instance.defaults.headers.common['Authorization'] = `Bearer ${AUTH_TOKEN}`;

  return instance
}

export default Request()