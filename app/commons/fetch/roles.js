import awaitTo from '../libs/awaitTo'
import Fetch from './index'

export const GetRoles = async () => {
  const Req = () => Fetch.get("/api/roles");
  const [err, response] = await awaitTo(Req)

  return { err, response }
}
