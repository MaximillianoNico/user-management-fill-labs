import awaitTo from '../libs/awaitTo'
import Fetch from './index'

export const SaveUser = async (payload) => {
  const Req = () => Fetch.post("/api/user/new", payload);
  const [err, response] = await awaitTo(Req)

  return { err, response }
}

export const UpdateUser = async (payload, id) => {
  const Req = () => Fetch.put(`/api/user/${id}`, payload);
  const [err, response] = await awaitTo(Req)

  return { err, response }
}

export const GetUsers = async () => {
  const Req = () => Fetch.get(`/api/users`);
  const [err, response] = await awaitTo(Req)

  return { err, response }
}

export const DeleteUser = async (id) => {
  const Req = () => Fetch.delete(`/api/user/${id}`);
  const [err, response] = await awaitTo(Req)

  return { err, response }
}
