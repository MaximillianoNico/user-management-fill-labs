import { deleteCookie } from "cookies-next";

const awaitTo = async (Request) => {
  try {
    const response = await Request();

    return [null, response]
  } catch (err) {
    if (err?.response?.status === 403) {
      deleteCookie("tkn", { path: "/" })

      window.location.href = "/login"
    }
    return [err, null]
  }
}

export default awaitTo;