import { Box } from '@chakra-ui/react';
import React from 'react';

const Loader = () => {
  return (
    <Box display="flex" justifyContent="center" width="100%" height="100vh" alignItems="center">
      <div className="dot_flashing"></div>
    </Box>
  )
}

export default Loader;