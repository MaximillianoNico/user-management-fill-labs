import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Box,
  Input,
  Tag,
  TagLabel,
  TagCloseButton,
  TagLeftIcon,
  useToast
} from '@chakra-ui/react'
import { AddIcon } from '@chakra-ui/icons'
import { useEffect, useState } from 'react'
import { SaveUser, UpdateUser } from '../../commons/fetch/users'

const ModalUser = ({ isOpen, onClose, isNew, masterData = [], user, onSuccess }) => {
  const toast = useToast()
  const [form, setForm] = useState({
    name: "",
    email: "",
    roles: []
  })

  useEffect(() => {
    // get master data
    if (!isNew && isOpen) setForm({
      name: !isNew && user ? user?.name : "",
      email: !isNew && user ? user?.email : "",
      roles: !isNew && user ? user?.roles : []
    })
  }, [isOpen])

  const handleInput = (e) => {
    const target = e?.target?.name

    if (target) setForm({
      ...form,
      [target]: e?.target?.value
    })
  }

  const handleSave = async() => {
    const { email, name, roles } = form
    const findRoleIds = roles.map((item) => {
      const findIdx = masterData?.find(itmRole => itmRole.name === item)
      return findIdx?.ID
    })

    const Request = isNew ? SaveUser : UpdateUser
    const { err } = await Request({ email, name, roles: findRoleIds }, user.ID)

    if (err) {
      toast({
        title: 'Failed Sign In',
        description: err?.response?.data?.message || "error sign in",
        status: 'error',
        duration: 9000,
        isClosable: true,
      })

      return;
    }

    toast({
      title: 'New User',
      description: isNew ? "success added data user" : "success updated data",
      status: 'success',
      duration: 4000,
      isClosable: true,
    })

    setTimeout(() => {
      onSuccess()
    }, 1000)
  }

  return (
    <Modal size="lg" isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{isNew ? "New User" : "User Detail"}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Input name="name" marginY={2} variant='flushed' placeholder='Name' defaultValue={form?.name} onChange={handleInput} />
          <Input name="email" marginY={2} variant='flushed' placeholder='Email' defaultValue={form?.email} onChange={handleInput} />
          <Box spacing={4}>
            {masterData.length && masterData.map(({ name, id }) => {
              const isMatch = (form?.roles || [])?.find((itm, idx) => itm == name && ({ idx, name }))
              if (isMatch) return (
                <Tag
                  marginTop={5}
                  size={"lg"}
                  key={"1"}
                  borderRadius='full'
                  variant='solid'
                  colorScheme='green'
                  onClick={() => {
                    const newRoles = form?.roles;
                    
                    setForm({
                      ...form,
                      roles: newRoles.splice(isMatch?.idx, 1)
                    })
                  }}
                >
                  <TagLabel>{name}</TagLabel>
                  <TagCloseButton />
                </Tag>
              )

              return (
                <Tag
                  onClick={() => {
                    setForm({
                      ...form,
                      roles: [...form?.roles, name]
                    })
                  }}
                  marginTop={5} size={"lg"} key={"1"} variant='subtle' colorScheme='white'>
                  <TagLeftIcon boxSize='12px' as={AddIcon} />
                  <TagLabel>{name}</TagLabel>
                </Tag>
              )
            })}
          </Box>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme='blue' mr={3} onClick={handleSave}>
            {isNew ? "Create" : "Save" }
          </Button>
          {/* <Button variant='ghost'>Secondary Action</Button> */}
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default ModalUser;
