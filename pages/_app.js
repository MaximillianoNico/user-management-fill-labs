import '../styles/globals.css'

import App from 'next/app'
import { ChakraProvider, extendTheme } from '@chakra-ui/react'
import { AuthProvider } from '../app/commons/contexts/auth'

const colors = {
  brand: {
    900: '#1a365d',
    800: '#153e75',
    700: '#2a69ac',
  },
}

export const theme = extendTheme({ colors })

const MyApp = ({ Component, pageProps, props }) => {
  // useEffect(() => {
  //   window.addEventListener('beforeinstallprompt', function(event) {
  //     // not show the default browser install app prompt
  //     event.preventDefault();
    
  //     // add the banner here or make it visible
  //     // …
    
  //     // save the event to use it later
  //     // (it has the important prompt method and userChoice property)
  //     window.promptEvent = event;
  //   });
  // }, [])

  // useEffect(() => {

  //   if (typeof window !== undefined) {
  //     // show the install app prompt
  //     window.promptEvent.prompt();

  //     // handle the Decline/Accept choice of the user
  //     window.promptEvent.userChoice.then((choiceResult: any) => {
  //       // hide the prompt banner here
  //       // …

  //       if (choiceResult.outcome === 'accepted') {
  //         console.info('mm User accepted the A2HS prompt');
  //       } else {
  //         console.info('mm User dismissed the A2HS prompt');
  //       }

  //       window.promptEvent = null;
  //     });
  //   }
  // }, [])
  return (
    <ChakraProvider>
      <AuthProvider pageProps={pageProps} ssrProps={props}>
        <Component {...pageProps} />
      </AuthProvider>
    </ChakraProvider>
  );
}

MyApp.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);
  const authToken = appContext?.ctx?.req?.headers?.cookie || null;

  console.log('SSR appProps: ', appProps);
  console.log('SSR appContext: ', appContext);

  return { props: { test: 1, authToken }, ...appProps }
}

export default MyApp
