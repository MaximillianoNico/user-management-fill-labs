import Table from 'rc-table';
import { Pen, Trash } from "phosphor-react";

import {
  Button,
  Box,
  Flex,
  useDisclosure,
  useToast,
  Text
} from '@chakra-ui/react'
import { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import { deleteCookie } from 'cookies-next'

import ModalUser from '../app/components/ModalUser';
import { GetRoles } from '../app/commons/fetch/roles';
import { DeleteUser, GetUsers } from '../app/commons/fetch/users';

const DashboardPage = () => {
  const _mounted = useRef()
  const { isOpen, onOpen, onClose } = useDisclosure()
  const router = useRouter()
  const toast = useToast()

  const [masterData, setMasterData] = useState([])
  const [users, setUsers] = useState([])
  const [activeUser, setActiveUser] = useState({});
  const [isNew, setIsNew] = useState(false);

  const columns = [
    {
      title: 'User',
      dataIndex: 'name',
      key: 'name',
      width: 300,
    },
    {
      title: 'Site Role',
      dataIndex: 'roles',
      key: 'roles',
      width: 300,
      render: (data) => (
        <div style={{ display: 'flex', columnGap: 10 }}>
          {data.filter(item => item !== "").map((chip, idx) => (
            <div style={{ borderRadius: 10, border: '1px solid grey', padding: '5px'}} key={idx}>{chip}</div>
          ))}
        </div>
      )
    },
    {
      title: 'Operations',
      dataIndex: '',
      key: 'operations',
      render: (data) => (
        <div style={{ display: 'flex', columnGap: 10 }}>
          <button onClick={() => {
            setActiveUser(data)
            onOpen()
          }}>
            <Pen size={16} color="#4a4a4a" />
          </button>
          <button onClick={() => {
            handleDeleteUser(data?.ID, data?.name)
          }}>
            <Trash size={16} color="#4a4a4a" />
          </button>
        </div>
      ),
    },
  ];

  // Refetch Users data table
  const RefetchUserData = async () => {
    const { response: dataUser, err: errUser } = await GetUsers()

    if (!errUser && !dataUser.length) {
      const reconstructUserData = (dataUser?.data?.data || []).map(({ roles = [], ...rest }) => ({
        ...rest,
        roles: (roles || []).map((item) => {
          const find = masterData.find(d => d.ID === item)

          if (find) return find.name
        })
      }))

      setUsers(reconstructUserData)
    }
  }
  
  // Get Users and Master Data
  useEffect(() => {
    const GetInitialData = async () => {
      const { response, err } = await GetRoles()
      const { response: dataUser, err: errUser } = await GetUsers()
      const resMasterData = response?.data?.data || [];

      if (!err) {
        setMasterData(response?.data?.data)
      }

      if (!errUser && !err && !users.length) {
        const reconstructUserData = (dataUser?.data?.data || []).map(({ roles = [], ...rest }) => ({
          ...rest,
          roles: (roles || []).flatMap((item) => {
            const find = resMasterData.find(d => d.ID === item)

            if (find) return find.name
          })
        }))

        setUsers(reconstructUserData)
      }

      _mounted.current = true
    }

    if (!_mounted.current) GetInitialData()
  }, [_mounted, users.length])

  const handleLogOut = () => {
    deleteCookie("tkn", { path: '/' });

    setTimeout(() => {
      router.push("/login")
    }, 2000)
  }

  // Handle delete user based on id
  const handleDeleteUser = async (id, name) => {
    const { err } = await DeleteUser(id) 

    if (err) {
      toast({
        title: 'Failed Sign In',
        description: err?.response?.data?.message || "error sign in",
        status: 'error',
        duration: 9000,
        isClosable: true,
      })

      return;
    }
    
    toast({
      title: 'Delete User',
      description: "success delete user: "+name,
      status: 'success',
      duration: 4000,
      isClosable: true,
    })

    RefetchUserData() // Refetch
  }

  const handleOpenNewModal = () => {
    setIsNew(true)
    onOpen()
  }

  const handleCloseModal = () => {
    if (isNew) setIsNew(false)
    onClose()
  }

  return (
    <div>
      <Box position={"absolute"} right={5} top={5}>
        <Button onClick={handleLogOut} colorScheme='blue'>LogOut</Button>
      </Box>
      <Box m="auto" display="flex" justifyContent="center" alignItems="center">
        <Box position="relative" marginTop={20}>
          <Flex columnGap={20}>
            <Text size={"lg"}>All Users</Text>
            <Box display="flex" alignItems="center">Total: {users.length}</Box>
            <Box position="absolute" right={0} top={0}>
              <Button colorScheme='blue' onClick={handleOpenNewModal}>New</Button>
            </Box>
          </Flex>
          <Box marginTop={10} overflowY="auto" maxH={400}>
            <Table columns={columns} data={users} />
          </Box>
        </Box>
      </Box>

      <ModalUser
        masterData={masterData}
        isNew={isNew}
        user={activeUser}
        isOpen={isOpen}
        onClose={handleCloseModal}
        onSuccess={() => {
          handleCloseModal();

          // refetch
          RefetchUserData()
        }}
      />
    </div>
  )
}

export const getServerSideProps = async (context) => {
  return {
    props: {
      protected: true,
    }, // will be passed to the page component as props
  }
}

export default DashboardPage;
