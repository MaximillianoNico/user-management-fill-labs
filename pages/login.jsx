import { useState, useCallback } from 'react';
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons'
import { useToast } from '@chakra-ui/react'
import { setCookies } from 'cookies-next';
import {
  InputRightElement,
  InputGroup,
  Button,
  Input,
  Flex,
  Stack,
  Text
} from '@chakra-ui/react'
import { useRouter } from 'next/router';
import { SignIn } from '../app/commons/fetch/auth';

const LoginPage = () => {
  const router = useRouter()
  const toast = useToast()

  const [showPwd, setShowPwd] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = useCallback(
    async () => {
      setIsLoading(true);

      const { err, response: dataLogin } = await SignIn(email, password)

      if (err) {
        toast({
          title: 'Failed Sign In',
          description: err?.response?.data?.message || "error sign in",
          status: 'error',
          duration: 9000,
          isClosable: true,
        })
        setIsLoading(false);

        return;
      }
      setCookies('tkn', dataLogin?.data?.data?.token , {
        domain: process.env.NODE_ENV === 'development' ? '' : 'personal-finance-app-flax.vercel.app' ,
        maxAge: 60 * 6 * 24
      })

      setTimeout(() => {
        setIsLoading(false);
        window.location.href = '/dashboard'
      }, 2000)
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [email, password]
  )

  const handleInput = (e) => {
    const value = e?.target?.value
    if (e?.target?.name === 'email') setEmail(value);
    else setPassword(value);
  }

  return (
    <Flex alignItems={'center'} justifyContent="center" m={'0'}>
          <Stack h={'100vh'}>
            <Stack spacing={3} mt="4rem">
              <Text fontSize='4xl'>Sign In</Text>
            </Stack>

            <Stack spacing={'24px'} mt="4rem !important">
              <Input
                pr='5rem'
                h="3rem"
                type='email'
                placeholder='Email'
                name="email"
                onChange={handleInput}
              />
              <InputGroup size='md'>
                <Input
                  pr='5rem'
                  h="3rem"
                  type={showPwd ? 'text' : 'password'}
                  placeholder='Enter password'
                  name="password"
                  onChange={handleInput}
                />
                <InputRightElement width='4.5rem' mt="1" mb="11">
                  <Button h='2.5rem' size='sm' onClick={() => setShowPwd(!showPwd)}>
                    {showPwd ? <ViewOffIcon /> : <ViewIcon />}
                  </Button>
                  
                </InputRightElement>
              </InputGroup>
            </Stack>

            <Stack mt="4rem !important">
              <Button
                isLoading={isLoading}
                colorScheme='blue'
                onClick={handleSubmit}
              >
                Sign In
              </Button>
            </Stack>
          </Stack>
        </Flex>
  )
}

export default LoginPage;
